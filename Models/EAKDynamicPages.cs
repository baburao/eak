
using System;


namespace EAK.Models
{
    public class EKDynamicPages
    {
        public int Id { get; set; }
        public String Title { get; set; }       
        public String Description { get; set;}      
        public DateTime CreatedDateTime { get; set; }


    }

}