
using System;

namespace EAK.Models
{
    public class EKYear
    {
        public int Id { get; set; }
        public String Year { get; set; }

    }
}