
using System;

namespace EAK.Models
{
    public class EKRecentUpdate
    {
        public int Id { get; set; }
        public int UpdateId { get; set; }
        public String Title { get; set; }
        public String ImagePath { get; set; }
        public String Description { get; set; }
        public String Type { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public String IsDeleted { get; set; }

    }
}