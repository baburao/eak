
namespace EAK.Models
{
    public class EKMessages
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Src { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }

}
