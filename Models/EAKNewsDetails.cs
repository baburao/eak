
using System;


namespace EAK.Models
{
    public class EKNewsDetails
    {
        public int Id { get; set; }
        public int NewsID { get; set; }
        public int LanguageID { get; set; }
        public string NewsTitle { get; set; }
        public string Description { get; set; }
        public string FullNews { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string IsArchive { get; set; }
        public string NewsTileImage { get; set; }

     
    }

}