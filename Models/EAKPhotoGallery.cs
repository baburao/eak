
using System;


namespace EAK.Models
{
    public class EKPhotoGallery
    {
        public int Id { get; set; }
        public String ImageName { get; set; }
        public String ImagePath { get; set; }
        public String Category { get; set; }
        public DateTime UploadedDate { get; set; }
        public String IsApproved { get; set; }
        public int ImageEventId { get; set; }
   

     
    }

}