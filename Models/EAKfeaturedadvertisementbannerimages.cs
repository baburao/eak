
using System;


namespace EAK.Models
{
    public class EKFeaturedadvertisementbannerimages
    {
        public int Id { get; set; }
        public int FeaturedAdvertisementBannerId { get; set; }
        public int PostingTypeId { get; set; }
        public String FeaturedAdvertisementBannerImageName { get; set; }
        public String Title { get; set; }
        public String Url { get; set; }


    }

}