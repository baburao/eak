using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace EAK.Models
{
    public class EAKContext : DbContext
    {
        public EAKContext(DbContextOptions<EAKContext> options): base(options){ }
        public DbSet<EKMessages> EKMessages { get; set; }
        public DbSet<EKNews> news { get; set; }
        public DbSet<EKNewsDetails> newsdetails { get; set; }
        public DbSet<EKRecentUpdate> recentupdates { get; set; }
        public DbSet<EKFeaturedadvertisementbannerimages> featuredadvertisementbannerimages { get; set; }
        public DbSet<EKCountries> countries { get; set; }
        public DbSet<EKDynamicPages> dynamicpages { get; set; }
        public DbSet<EKPhotoGallery> photogallery { get; set; }
        public DbSet<EKYear> years { get; set; }
    }


}