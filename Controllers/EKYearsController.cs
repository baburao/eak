using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;
using System;

namespace EAK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EKYearsController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKYearsController(EAKContext context)
        {
            _context = context;
        }

        //Get Year
        [HttpGet("getYear")]
        public IEnumerable<EKYear> getYear()
        {
            var data = _context.years;

            return data;


        }

        //Add Year
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> addYear(EKYear EY)
        {
            if (ModelState.IsValid)
            {
                await _context.years.AddAsync(EY);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction("Index");

        }




    }


}