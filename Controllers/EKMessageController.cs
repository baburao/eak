using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;

namespace EAK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EKMessageController : ControllerBase
    {
    private readonly EAKContext _context;

    public EKMessageController(EAKContext context)
    {
        _context = context;
    }

    // GET: api/Teams
    [HttpGet("getmessages")]
    public IEnumerable<EKMessages> GetCities()
    {
        var data =  _context.EKMessages;
        return data;
    }

    [HttpPost]

    public async Task<IActionResult> SaveLoanContract(EKMessages lc)
    {
        if (ModelState.IsValid)
        {
            await _context.EKMessages.AddAsync(lc);
            await _context.SaveChangesAsync();

            // var existingBrand = _context.EKMessages.Find(672);
            // existingBrand.Description = "Updated Brand";
            // await _context.SaveChangesAsync();

            // var brandToDelete = _context.Find<EKMessages>(672);
            // _context.EKMessages.Remove(brandToDelete);
            // await _context.SaveChangesAsync();
        }
        return RedirectToAction("Index");


    }

    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> PostCustomerAndOrder(EKMessages lc)
    {
        if (ModelState.IsValid)
        {
            await _context.EKMessages.AddAsync(lc);
            await _context.SaveChangesAsync();

            // var existingBrand = _context.EKMessages.Find(672);
            // existingBrand.Description = "Updated Brand";
            // await _context.SaveChangesAsync();

            // var brandToDelete = _context.Find<EKMessages>(672);
            // _context.EKMessages.Remove(brandToDelete);
            // await _context.SaveChangesAsync();
        }
        return RedirectToAction("Index");
    }

    }

}