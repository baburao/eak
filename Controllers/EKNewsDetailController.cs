using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;
using System;


namespace EAK.Controllers
{
    // Getting News

    [Route("api/[controller]")]
    [ApiController]
    public class EKNewsDetailController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKNewsDetailController(EAKContext context)
        {
            _context = context;
        }

        [HttpGet("getNewsDetails")]
        public IEnumerable<EKNewsDetails> getNewsDetails()
        {
            var data = _context.newsdetails;
            return data;
        }



        // Inserting News Details
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> addNewsDetails(EKNewsDetails END)
        {
            if (ModelState.IsValid)
            {
                await _context.newsdetails.AddAsync(END);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction("Index");

        }


        // Updating NewsDetalis
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> upDateNewsDetails(EKNewsDetails END)
        {
            if (ModelState.IsValid)
            {
                var editNews = _context.newsdetails.Find(END.Id);
                editNews.Description = END.Description;
                editNews.FullNews = END.FullNews;
                editNews.UpdatedDate = Convert.ToDateTime(END.UpdatedDate);
                editNews.UpdatedBy = END.UpdatedBy;
                editNews.NewsTileImage = END.NewsTileImage;
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }



        // Delete News
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> deleteNewsDetails(EKNewsDetails END)
        {
            if (ModelState.IsValid)
            {

                var removeNewsDetails = _context.Find<EKNewsDetails>(END.Id);
                _context.newsdetails.Remove(removeNewsDetails);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction("Index");
        }

    }

}
