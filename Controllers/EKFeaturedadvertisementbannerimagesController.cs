using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;

namespace EAK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EKFeaturedadvertisementbannerimagesController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKFeaturedadvertisementbannerimagesController(EAKContext context)
        {
            _context = context;
        }


        // Get Banner Images
        [HttpGet("getBannerImages")]
        public IEnumerable<EKFeaturedadvertisementbannerimages> getBannerImages()
        {
            var data = _context.featuredadvertisementbannerimages;
            return data;
        }


        // Get Banner Images by Id
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<EKFeaturedadvertisementbannerimages>> getBannerById(EKFeaturedadvertisementbannerimages EFDB)
        {
            var getBannerById = await _context.featuredadvertisementbannerimages.FindAsync(EFDB.Id);

            if (getBannerById == null)
            {
                return NotFound();
            }

            return getBannerById;
        }
    }

}