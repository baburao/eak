using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;

namespace EAK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EKCountriesController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKCountriesController(EAKContext context)
        {
            _context = context;
        }

     // Get Countries
        [HttpGet("getCountries")]
        public IEnumerable<EKCountries> getCountries()
        {
            var data = _context.countries;
            return data;
        }
        
    }

}