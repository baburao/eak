using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;
using System;


namespace EAK.Controllers
{
    // Getting News

    [Route("api/[controller]")]
    [ApiController]
    public class EKNewsController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKNewsController(EAKContext context)
        {
            _context = context;
        }

        [HttpGet("getnews")]
        public IEnumerable<EKNews> GetNews()
        {
            var data = _context.news;
            return data;
        }



        // Inserting News
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> addNews(EKNews EN)
        {
            if (ModelState.IsValid)
            {
                await _context.news.AddAsync(EN);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction("Index");

        }


        // Updating News
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> upDateNews(EKNews EN)
        {
            if (ModelState.IsValid)
            {
                var editNews = _context.news.Find(EN.Id);
                editNews.NewsDate = Convert.ToDateTime(EN.NewsDate);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }



        // delete news

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> deleteNews(EKNews EN)
        {
            if (ModelState.IsValid)
            {

                var removeNews = _context.Find<EKNews>(EN.Id);
                _context.news.Remove(removeNews);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction("Index");
        }

    }

}
