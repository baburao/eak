using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;
using System;

namespace EAK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EKPhotoGalleryController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKPhotoGalleryController(EAKContext context)
        {
            _context = context;
        }
        // get photo gallery

        [HttpGet("getPhotoGallery")]
        public IEnumerable<EKPhotoGallery> getPhotoGallery()
        {
            var data = _context.photogallery;
            return data;
        }

        // Add photo gallery
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> addPhotoGallery(EKPhotoGallery EPG)
        {
            if (ModelState.IsValid)
            {
                await _context.photogallery.AddAsync(EPG);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction("Index");

        }

        // Update Photo gallery
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> upDatePhotoGallery(EKPhotoGallery EPG)
        {
            if (ModelState.IsValid)
            {
                var editPhotoGallery = _context.photogallery.Find(EPG.Id);
                editPhotoGallery.ImageName = EPG.ImageName;
                editPhotoGallery.ImagePath = EPG.ImagePath;
                editPhotoGallery.UploadedDate = Convert.ToDateTime(EPG.UploadedDate);
                editPhotoGallery.Category = EPG.Category;
                editPhotoGallery.IsApproved = EPG.IsApproved;
                editPhotoGallery.ImageEventId = EPG.ImageEventId;
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        // Get Photo Gallery by Id
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<EKPhotoGallery>> getPhotoGalleryById(EKPhotoGallery EPG)
        {
            var getPhotoGalleryById = await _context.photogallery.FindAsync(EPG.Id);

            if (getPhotoGalleryById == null)
            {
                return NotFound();
            }

            return getPhotoGalleryById;
        }


    }

}