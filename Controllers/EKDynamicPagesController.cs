using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;
using System;

namespace EAK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EKDynamicPagesController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKDynamicPagesController(EAKContext context)
        {
            _context = context;
        }

        // Get Dynamic Pages
        [HttpGet("getDynamicPage")]
        public IEnumerable<EKDynamicPages> getDynamicPage()
        {
            var data = _context.dynamicpages;
            return data;
        }
        // Add Dynamic Page
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> addDynamicPage(EKDynamicPages EDP)
        {
            if (ModelState.IsValid)
            {
                await _context.dynamicpages.AddAsync(EDP);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction("Index");

        }
        // Update Dynamic Page
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> upDateDynamicPage(EKDynamicPages EDP)
        {
            if (ModelState.IsValid)
            {
                var editDynamicContent = _context.dynamicpages.Find(EDP.Id);
                editDynamicContent.Description = EDP.Description;
                editDynamicContent.Title = EDP.Title;
                editDynamicContent.CreatedDateTime = Convert.ToDateTime(EDP.CreatedDateTime);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        // Remove Dynamic Page
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> deleteDynamicPage(EKDynamicPages EDP)
        {
            if (ModelState.IsValid)
            {

                var removeDynamicPage = _context.Find<EKDynamicPages>(EDP.Id);
                _context.dynamicpages.Remove(removeDynamicPage);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        // Get Dynamic Page By Id
        [HttpPost]
        [Route("dynamicPage")]
        public async Task<ActionResult<EKDynamicPages>> dynamicPage(EKDynamicPages EDP)
        {
            var getDynamicPageById = await _context.dynamicpages.FindAsync(EDP.Id);

            if (getDynamicPageById == null)
            {
                return NotFound();
            }

            return getDynamicPageById;
        }




    }

}