using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using EAK.Models;
using System;

namespace EAK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EKRecentUpdateController : ControllerBase
    {
        private readonly EAKContext _context;

        public EKRecentUpdateController(EAKContext context)
        {
            _context = context;
        }

        //Get Recent Update
        [HttpGet("GetRecentUpdate")]
        public IEnumerable<EKRecentUpdate> GetRecentUpdate(EKRecentUpdate ERU)
        {
            var data = _context.recentupdates;

            return data;


        }



    }


}